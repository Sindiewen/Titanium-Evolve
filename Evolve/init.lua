TERM_X, TERM_Y = term.getSize()
if pocket or turtle then
    error("Titanium Evolve currently only supports advanced computers, not " .. ( pocket and "pocket computers" or "turtles" ), 0)
elseif not( term.isColour or term.isColour() ) then
    error("Titanium Evolve requires an advanced computer to run", 0)
end

function execute( path, env )
    path = path:find "%.lua$" and path or ( path .. ".lua" )

    local ok, err = loadfile( path, env )
    if not ok then return error("Failed to execute '"..path.."': "..tostring( err )) end

    return ok()
end

function require( _path )
    local env = setmetatable( {}, { __index = _ENV } )
    execute( fs.combine( "/Evolve", _path ):gsub( "%.", "/" ), env )

    env._ENV = nil
    if next( env ) then _G[ _path:gsub(".+%.", "") ] = env end
end

require "states.loading"
loading.splash()
