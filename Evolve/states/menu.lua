--[[
    Menu state: Configure logic for menu screen, including TML importing
]]

EvolveApp:importFromTML "Evolve/ui/welcome.tml"
EvolveApp:cache( "pages", "PageContainer" )

local pages = EvolveApp:get "pages"
-- TODO: Menu options
-- TODO: Section management
